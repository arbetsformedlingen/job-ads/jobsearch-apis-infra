# jobsearch-apis infra

This repository contains files regarding build and deployment
pipelines for the
[jobsearch repository](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/tree/develop).

## More doc

To create a personal environment within OpenShift read [personal-environment doc](docs/personal-environment.md).

To get help with troubleshooting, read the [troubleshooting doc](docs/troubleshooting.md).

## Directories

In the examples directory are template files to for needed secrets.

The argoocd directory contains files to activate ArgoCD deployments.

