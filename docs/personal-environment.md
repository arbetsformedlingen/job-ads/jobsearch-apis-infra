# Personal environment

Purpose of this document is to describe how to set up a personal environment to test and
evaluate a branch of jobsearch-api.

Assumptions on these instructions is that you are working in jobsearch-apis repository
on a branch `myfeature` and your AF-id is `demoo`.

You need to have OpenShift CLI tool `oc` installed, follow RedHats [instructions](https://docs.openshift.com/container-platform/4.7/cli_reference/openshift_cli/getting-started-cli.html) if you not already have it installed.

**Note:** Right now not fully functional, need to figure out full bootstrap from zero. Though, can easily be pointing to already existing indexes.

## First time

1. Login to the OpenShift test cluster with `oc login https://api.test.services.jtech.se:6443`
and follow the instructions. Your credentials are the LDAP user and password,
same as you use to login to OpenShift console and Nexus.
1. Create a new personal project/namespace in Openshift with the command
  `oc new-project demoo-jobsearch-apis`. Replace `demoo` with your AF id.
1. Checkout [the infrastructure repository](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis-infra).
1. Go to the `examples` directory.
1. Copy the file `taxonomy-secret.yaml` to a directory
  outside the repository so you avoid checking in secrets by mistakes.

Your namespace is now prepared to install your personal copy of jobsearch-apis.

## Deploy jobsearch-apis

1. Push your branch of jobsearch-apis to gitlab.
2. Find out the last commit sha for your branch. Write down the seven first letters in the sha.
3. Update your copy of [the infrastructure repository](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis-infra).
4. Ensure your personal namespace you created the first time is
   active doing `oc project demoo-jobsearch-apis`
5. Go down to `kustomize/overlays/personal`
6. Edit the file in `kustomization.yaml`, change the value of the field `newTag`
   to the commit sha of the last commit. (Seven first characters.)
7. Apply the application: `oc apply -k .` (If oc do not recognize flag `-k` or complain on the files, you may need to upgrade oc.)
8. Application is now starting.
9. Either check status of the deploy with `oc get deployment`
   or look in the OpenShift console.
10. To find out how to access the services, do `oc get routes`
   and access them with the value in `HOST/PORT` column.

When you want to update your personal environment to run a new commit. Just iterate the list above.

## Clean up

When you are done, remember to clean up. Do `oc delete project demoo-jobsearch-apis`.
