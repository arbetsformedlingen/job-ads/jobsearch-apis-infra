# Troubleshooting

Purpose of this document is to support troubleshooting of deployments and be
able to understand what is happening and what is running.

The following environments are supported:

| Environment | OpenShift Cluster | Namespace                 | Tag     |
|-------------|-------------------|---------------------------|---------|
| Develop     | test              | jobsearch-apis-develop    | *HEAD/main* |
| Staging     | test              | jobsearch-apis-staging    | staging |
| i1          | test              | jobsearch-apis-i1         | staging |
| t2          | test              | jobsearch-apis-t2         | staging |
| Production  | prod              | jobsearch-apis-prod       | prod    |
| Standby     | standby           | jobsearch-apis-prod-stdby | prod    |

## Merge requests and branches

Each push to a branch in GitLab is automatically initializing a build. The outcome of the
build are an image tagged with two tags: branch name and commit sha.

Easiest way to see the build progress and log is to create a Merge Request. (Mark it as draft if
it is not ready for review.)
In your merge request an box similar to the following image shows up.
[![Ongoing build shown in GitLab](images/aardvark-building-in-gitlab.png)](images/aardvark-building-in-gitlab.png)
This is an indication that a build is ongoing. If the symbol is a check mark, the build is ready and have passed.
An X mark shows the build failed.

To see what is happening during or after the build. Click on the right symbol
and select "ci/aardvark" in the popup menu appearing. You will be redirected to the page showing your build.
In Tekton/Aardvark language pipeline run.

[![Finished build of a branch](images/aardvark-pipeline-build-branch.png)](images/aardvark-pipeline-build-branch.png)

When building a branch the following steps are taken:

1. Signaling to gitlab how to find the build.
1. Checkout the branch.
1. Building container image with the commit sha as tag and push to Nexus.
1. Tag image with branch name and push to Nexus.
1. Tell GitLab build is ready.

The grey steps are not happening when building other branches than main.

To see the logs for a step, click the on step in the image.

### Common issues to failure build a branch

In step build-image, it is probably something in the `Dockerfile` causing it
to fail. Verify you can build it locally and read the logs for the build-image
step.

In the step tag-image-with-branch, the cause is probably the name of the branch
is not a valid container image tag name. Try to use only a-z, 0-9 and dash(`-`) in
your branch name.

For the other steps, it is probably an intermittent issue. Click on the Actions
button and then ReRun.

## Deploy to Develop

Once a merge request is merged a new build is triggered. The outcome of the
build are an image tagged with two tags: 'main', 'latest', and commit sha.

All the steps running when building the branch do also run for main. In addition
to the steps when building branch the following steps are running:

1. Check if there is an infrastructure repository.
1. Checkout the infrastructure repository
1. Set the the develop environment to use the image with commit sha of the code repository.
1. Commit to infrastructure repository.
1. An extra step to tag the build container image with `latest` is also done.

When the commit to the infrastructure repository is pushed to GitLab a webhook trigger
Aardvarks deploy-pipeline. You can find your deployment to develop running in the [Aardvark dashboard over deployments](https://console-openshift-console.test.services.jtech.se/k8s/ns/aardvark/tekton.dev~v1beta1~Pipeline/.aardvark-deployer-pipeline/Runs?orderBy=desc&sortBy=Started). Look after the lastest run that starts with the
repository name.

Once the pipeline is ready, you shall se the new version of the service starting up. See the [workload of pods in the develop namespace](https://console-openshift-console.test.services.jtech.se/k8s/ns/jobsearch-apis-develop/pods).
Click on the newly created pod and look under the section of *Containers*. You shall see that it run an image
with the tag named with same as our merge commit in the source code repository.

### Common issues to failure deploy to develop

Main issues are the same as when building a branch. If any of the other steps
fails, click on the Actions button and then ReRun.

## Deploy to all, but develop

For all other environments:

* staging (also replicated for i1 and t2)
* prod

are deployed setting a tag named as the environment. To make it concrete we look at deploying to staging.

If there already is a tag `staging`, delete it. The deletion of the tag will not impact what
is currently running in staging.

Tag the commit you want to install on staging.
Wait for about 5 minutes and it shall have at least started to roll out. The rollout time depends very much on the
number of pods and their start up time.

Go to the OpenShift console, select the project/namespace for the environment, Select *Workloads* and then *Pods*.
Click on the newly created pod and look under the section of *Containers*. You shall see that it run an image
with the tag named with same as our merge commit in the source code repository.

### Common issues to failure deploy

Verify that the infrastructure repository has got updated in
`kustomize/overlays/ENVIRONMENT/kustomization.yaml` with the correct commit sha for the field
`newTag`. If not, go to the
[list of lately running release pipelines](https://console-openshift-console.test.services.jtech.se/k8s/ns/aardvark/tekton.dev~v1beta1~Pipeline/aardvark-release-pipeline/Runs)
 and find the last run for your infrastructure repository.
All steps shall have succeeded. Otherwise click on the Actions
button and then ReRun.

If the repository infrastructure repository got updated, wait for at least 15 minutes.
If nothing have started updating in the namespace, talk to Calamari.
We are working on to provide a dashboard to help trouble shooting.
