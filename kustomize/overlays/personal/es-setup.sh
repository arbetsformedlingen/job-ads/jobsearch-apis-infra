#!/bin/bash

ES_HOST=elastic
ES_PORT=9200

# Create synonym indexes
curl -X PUT http://${ES_HOST}:${ES_PORT}/jae-synonym-dictionary-index
curl -X POST http://${ES_HOST}:${ES_PORT}/_aliases -H "Content-Type: application/json" -d '{"actions": [{"add": {"index": "jae-synonym-dictionary-index", "alias": "jae-synonym-dictionary"}}]}'

curl -X PUT http://${ES_HOST}:${ES_PORT}/platsannons-read
# Jobad-index and aliases.
#curl -X PUT http://${ES_HOST}:${ES_PORT}/platsannons-historical
#curl -X PUT http://${ES_HOST}:${ES_PORT}/platsannons
#curl -X POST http://${ES_HOST}:${ES_PORT}/_aliases \
#  -H "Content-Type: application/json" \
#  -d '{"actions": [{"add": {"index": "platsannons", "alias": "platsannons-read"}},{"add": {"index": "platsannons", "alias": "platsannons-write"}}]}'

# Create API-keys. NB. Will delete all keys in /pb, /bulk, /taxonomy !
curl -X PUT http://${ES_HOST}:${ES_PORT}/apikeys/_doc/pb -H "Content-Type: application/json" -d '{"demo": {"id": 1, "app": "local dev"}}'
curl -X PUT http://${ES_HOST}:${ES_PORT}/apikeys/_doc/bulk -H "Content-Type: application/json" -d '{"demo": {"id": 1, "app": "local dev"}}'
curl -X PUT http://${ES_HOST}:${ES_PORT}/apikeys/_doc/taxonomy -H "Content-Type: application/json" -d '{"demo": {"id": 1, "app": "local dev"}}'
